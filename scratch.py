In [1]: import librosa
imp
In [2]: import cPickle as pickle

In [3]: import os

In [4]: bpm_mappings = {}

In [5]: audio_filenames = os.listdir("wav")

In [6]: for audio_filename in audio_filenames:
   ...:     y, sr = librosa.core.load(os.path.join("wav", audio_filename))
   ...:     bpm = librosa.beat.tempo(y, sr)[0]
   ...:     bpm_mappings[audio_filename] = bpm
   ...:     output_filename = os.path.join("stretched", audio_filename[:-4] + "_stretched.wav")
   ...:     command = "rubberband --tempo {:.1f}:100 {} {}".format(
   ...:         bpm, os.path.join("wav", audio_filename), output_filename)
   ...:     os.system(command)
