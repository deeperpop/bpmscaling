import librosa, pickle, os

CWD =  os.path.dirname(os.path.abspath(__file__))
TARGET_BPM = 100.0

def main():
    bpmMappings = {}
    infileNames = os.listdir("data")
    for filename in infileNames: #Read in Files  
        y, sr = librosa.core.load("data/" + filename) #TODO: may be able to reduce duration
        bpm = librosa.beat.tempo(y, sr)[0] #Find BPM
        bpmMappings[filename] = bpm
        outfile = "/scaledData/" + filename[:-4] + "_scaled.wav"
        target_scale = bpm / TARGET_BPM
        #scale file and save to scaledData
        scaleFile(target_scale, 1.0, CWD+"/data/"+filename, CWD+outfile) 

    #Save name:bpm mapping in pickle
    pickle.dump(bpmMappings, open("bpmMappings.pickle", "wb"))


def scaleFile(timeRatio, pitchRatio, infile, outfile):
    cmd = "{}/rubberbandExec/rubberband.exe -t {:.1f} -p {:.1f} {} {}".format(
       CWD, timeRatio, pitchRatio, infile, outfile)
    print cmd
    os.system(cmd)

if __name__ == "__main__":
    main()