In [1]: import librosa

In [2]: librosa.output
Out[2]: <module 'librosa.output' from '/usr/local/lib/python2.7/dist-packages/librosa/output.pyc'>

In [3]: librosa.core
Out[3]: <module 'librosa.core' from '/usr/local/lib/python2.7/dist-packages/librosa/core/__init__.pyc'>

In [4]: clear


In [5]: stretched_folder = "stretched"

In [6]: ss_folder = "stretched-sliced"

In [7]: import os

In [8]: slice_length = 8 * 16000
   ...: for filename in os.listdir(stretched_folder):
   ...:     filepath = os.path.join(stretched_folder, filename)
   ...:     song_id = filename[:-4]
   ...:     audio, _ = librosa.core.load(filepath, sr=16000, mono=True)
   ...:     for index, end in enumerate(range(slice_length, len(audio), slice_length)):
   ...:         audio_slice = audio[(end - slice_length):end]
   ...:         target_filename = song_id + str(index) + '.wav'
   ...:         target_filepath = os.path.join(ss_folder, target_filename)
   ...:         librosa.output.write_wav(target_filepath, audio_slice, 16000)
   ...:         

In [9]: 
